@extends('adminlte.master');

@section ('content')
<div class="card card-primary m-3">
         <div class="card-header">   
                <h3 class="card-title">Create Question </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method ="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control" name="judul" id="judul" value = "{{old('title', '')}}" placeholder="Enter your Title Question">
                    @error('judul')
                        <div class= "alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Content</label>
                    <textarea class="form-control" rows = "3" name ="isi" id="isi" value ="" placeholder="Enter your Question"> {{old('isi', '')}} </textarea>
                  </div>
                  @error('isi')
                    <div class="alert alert-danger">
                            {{ $message }}
                    </div>
                    @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>      

@endsection('content')
