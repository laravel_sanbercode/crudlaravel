@extends('adminlte.master');

@section ('content')
<div class="card card-primary m-3">
         <div class="card-header">   
                <h3 class="card-title">Edit Question {{$post->id}} </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$post->id}}" method ="POST">
              @csrf
              @method('PUT') 
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control" name="judul" id="judul" value = "{{old('title', $post->judul)}}" placeholder="Enter your Title Question" required>
                    @error('judul')
                        <div class= "alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Content</label>
                    <textarea class="form-control" rows = "3" name ="isi" id="isi" value ="" placeholder="Enter your Question"> {{old('isi', $post->isi)}} </textarea>
                  </div>
                  @error('isi')
                    <div class="alert alert-danger">
                            {{ $message }}
                    </div>
                    @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>      

@endsection('content')
