@extends('adminlte.master')

@section('content')

<div class= "mt-3 mr-1 ml-1">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Questions Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
              <a class = "btn btn-primary mb-2" href ="/pertanyaan/create"> Create New Question</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $question)
                        <tr>
                            <td> {{$key +1 }} </td>
                            <td> {{$question->judul}} </td>
                            <td> {{$question->isi}} </td>
                            <td style ="display : flex; ">
                                <a href="/pertanyaan/{{$question->id}}" class="btn btn-info btn-sm mr-1">Show</a>
                                <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-primary btn-sm mr-1">Edit</a>
                                <form action="/pertanyaan/{{$question->id}}" method ="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class = "btn btn-danger">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                            <td colspan='4' align='center'> No Data</td>
                            </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>

@endsection('content')
