@extends ('adminlte.master')

@section('content')
<div class="m-2 p-3">
<h1>Show Post {{$post->id}}</h1>
    <div class="callout callout-success ml-4">
      <h2>{{$post->judul}}</h2>
        <p>{{$post->isi}}</p>
    </div>
</div>

<div class="card-footer">
                  <a href="/pertanyaan" class="btn btn-info">Back!</a>
                </div>


<h4></h4>
<p></p>

@endsection